#!/usr/bin/env python
import logging
import os
import re
import yaml
from datetime import datetime
from typing import Tuple, Optional, Union

import click


from nbrun.nbrunner import NBRunner
from nbrun.utils.log_settings import init_logging

__author__ = 'Datatree'

init_logging()
logger = logging.getLogger(__name__)


def parse_nbrun_option(option, nbarg_regexp_str) -> Tuple[str, Optional[Union[str, int, datetime]]]:
    groups = re.match(nbarg_regexp_str, option)
    if not (bool(groups)):
        raise ValueError(f"CLI option {option} is not valid nbarg option ({nbarg_regexp_str}).")

    key = groups[1]
    value = yaml.load(groups[2])

    return key, value


@click.command(help="Run all notebooks in directory")
@click.argument("procdir", type=click.Path(exists=True), required=True)
@click.argument("runsdir", type=click.Path(), required=False)
@click.option('--kernel', type=click.STRING, default=None, help='existing IPython kernel name')
@click.option('--nbparams', type=click.STRING, default="params.yaml", help="Filename for notebook's params. Default: params.yaml in $runsdir.")
@click.option('--rerun', type=click.STRING, default=None, help='run identification in format YYYY-MM-DD-NNN')
@click.option('--latest', flag_value='latest', help='use latest run identification')
@click.option('--filter', '-f', type=click.STRING, default=None, help='filter steps with pattern like: 1,3-10,^5-6')
@click.option('--nbarg', ' -n', type=click.STRING, default=[], multiple=True)
def nbr(procdir, runsdir, kernel, nbparams, rerun, latest, filter, nbarg):

    root_directory = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
    if rerun is not None and latest:
        raise ValueError("Either <rerun> or <latest> option must be used. Cannot set values for both options.")

    if runsdir is None:
        runsdir = os.path.join(procdir, 'runs')

    notebook_args = {}
    if len(nbarg) > 0:
        nbarg_regexp_str = r"^([A-Za-z0-9\.-_]+)=(.+)$"
        for arg in nbarg:
            key, value = parse_nbrun_option(arg, nbarg_regexp_str)
            notebook_args[key] = value

    nbrunner = NBRunner(
        procdir, runsdir, root_directory,
        kernel,
        nbparams,
        rerun, latest == 'latest',
        filter,
        notebook_args
    )
    nbrunner.process()


if __name__ == '__main__':
    nbr()
