import logging
import os
import pickle
from shutil import copyfile
from typing import Any

import yaml

__author__ = 'Datatree'
logger = logging.getLogger(__name__)


def _directory_exists(directory):
    if bool(directory) and os.path.exists(directory) and os.path.isdir(directory):
        return True
    return False


def _filename_exists(filename):
    if filename is not None and filename != '' and os.path.exists(filename) and os.path.isfile(filename):
        return True
    return False


def file_exists(directory: str = None, filename: str = None):
    """Check if a file exists in a directory."""
    if bool(directory):
        if not _directory_exists(directory):
            return False

        if bool(filename):
            full_filename = os.path.join(directory, filename)
            return _filename_exists(full_filename)
        else:
            return True  # directory exists, filename not set
    else:
        if bool(filename):
            return _filename_exists(filename)
        else:
            return False  # neither directory nor filename provided


def get_dirname(path):
    res = os.path.realpath(os.path.dirname(path))
    return res


def makedirs(directory):
    if os.path.exists(directory) and os.path.isdir(directory):
        return
    os.makedirs(directory, exist_ok=True)


def recreate_file(directory, filename, content=None):
    full_filename = os.path.join(directory, filename)
    try:
        os.remove(full_filename)
    except OSError:
        pass

    if content is None:
        content = ''

    with open(full_filename, 'a') as f:
        f.write(content)


def load_file(directory: str = None, filename: str = None, mode: str = ''):
    """Load content of a file in campaign's directory."""
    if directory is not None and filename is not None:
        full_filename = os.path.join(directory, filename)
    elif directory is None:
        full_filename = filename
    elif filename is None:
        full_filename = directory
    else:
        raise ValueError("Cannot load file if directory and filename are both None.")

    if not os.path.exists(full_filename):
        raise ValueError("Cannot find campaign's file '{}' (expected full path: '{}').".format(filename, full_filename))

    if not os.path.isfile(full_filename):
        raise ValueError("Filename '{}' - full path '{}' is expected to be a file.".format(filename, full_filename))

    with open(full_filename, 'r' + mode) as content_file:
        content = content_file.read()

    return content


def save_file(directory: str = None, filename: str = None, content: str = None, mode: str = '', rewrite: bool = True) -> None:
    if directory is not None and filename is not None:
        full_filename = os.path.join(directory, filename)
    elif directory is None:
        full_filename = filename
    elif filename is None:
        full_filename = directory
    else:
        raise ValueError("Cannot load file if directory and filename are both None.")

    if not rewrite and os.path.exists(full_filename):
        raise ValueError("File {} (full path {}) already exists. Cannot overwrite existing file. Stored content: {}")

    with open(full_filename, 'w' + mode) as f:
        f.write(content)


def load_from_yaml_file(directory: str = None, filename: str = None) -> Any:
    yaml_content = load_file(directory, filename)
    try:
        res_yaml = yaml.load(yaml_content)
    except yaml.YAMLError:
        raise

    return res_yaml


def save_to_yaml_file(directory: str = None, filename: str = None, content: Any = None) -> None:
    if directory is not None and filename is not None:
        full_filename = os.path.join(directory, filename)
    elif directory is None:
        full_filename = filename
    elif filename is None:
        full_filename = directory
    else:
        raise ValueError("Cannot load file if directory and filename are both None.")

    try:
        with open(full_filename, 'w') as yaml_file:
            yaml.dump(content, yaml_file, default_flow_style=False)
    except yaml.YAMLError:
        raise


def load_from_pickled_file(directory: str = None, filename: str = None) -> Any:
    """Load environment settings from campaign's directory."""
    pickled_content = load_file(directory, filename, mode='b')
    unpickled_content = pickle.loads(pickled_content)
    return unpickled_content


def save_to_pickled_file(directory: str = None, filename: str = None, content: Any = None) -> None:
    """Load environment settings from campaign's directory."""
    pickled_content = pickle.dumps(content, protocol=pickle.HIGHEST_PROTOCOL)
    save_file(directory, filename, pickled_content, mode='b')


def copy_file(source, destination):
    logger.debug("Copy %s to %s.", source, destination)
    copyfile(source, destination)
