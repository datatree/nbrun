import os
import sys
import logging
import logging.handlers


__author__ = 'Datatree'
logger = logging.getLogger(__name__)


MEMORY_HANDLER_CAPACITY = 4096 * 1000

logfile_log_level = logging.DEBUG
stdout_log_level = logging.INFO

log_format = (
    '%(asctime)s - '
    '%(processName)s - '
    '%(levelname)s - '
    '%(name)s:%(lineno)s - '
    '%(message)s'
)

formatter = logging.Formatter(log_format)


def init_logging():

    streamhandler = logging.StreamHandler(sys.stderr)
    streamhandler.setLevel(logfile_log_level)
    streamhandler.setFormatter(formatter)

    memoryhandler = logging.handlers.MemoryHandler(
        capacity=MEMORY_HANDLER_CAPACITY,
        flushLevel=logging.CRITICAL,
        target=streamhandler
    )
    memoryhandler.setLevel(logfile_log_level)

    root_logger = logging.getLogger()
    root_logger.setLevel(logfile_log_level)
    root_logger.addHandler(memoryhandler)


def reset_logging(directory, filename):
    logfile = os.path.join(directory, filename)
    logger.debug("Redirecting logging to file '%s'", logfile)

    fileh = logging.FileHandler(logfile, 'a')
    fileh.setLevel(logfile_log_level)
    fileh.setFormatter(formatter)

    log = logging.getLogger()  # root logger
    memory_handler = None
    for hdlr in log.handlers:  # remove all old handlers
        if isinstance(hdlr, logging.handlers.MemoryHandler):
            memory_handler = hdlr
            break

    memory_handler.setTarget(fileh)
    memory_handler.flush()

    for hdlr in log.handlers[:]:  # remove all old handlers
        log.removeHandler(hdlr)

    log.addHandler(fileh)

    stdouthandler = logging.StreamHandler(sys.stdout)
    stdouthandler.setLevel(stdout_log_level)
    stdouthandler.setFormatter(formatter)
    log.addHandler(stdouthandler)
