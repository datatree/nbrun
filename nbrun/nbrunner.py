import glob
import logging
import os
import re
from datetime import datetime, date
import papermill as pm
from typing import List, Dict, Tuple

from nbrun.nbparams import NBParams
from nbrun.utils.fs import recreate_file, copy_file, file_exists, load_from_yaml_file, save_to_pickled_file
from nbrun.utils.log_settings import reset_logging
from nbrun.naming import NBRunnerNaming

__author__ = 'Datatree'
logger = logging.getLogger(__name__)


class NBRunnerStep:

    def __init__(self, name):
        self.name = name
        self.number = int(re.split(r"[ _]", self.name, 1)[0])
        self.base_name = os.path.splitext(self.name)[0]
        self.enabled = True


class NBRunner:

    def __init__(self, procdir, rundir, root_directory, kernel, nbparams, rerun, latest, filter, cli_args):
        self.nbrundir = os.path.split(os.path.split(os.path.abspath(__file__))[0])[0]

        _, procname = os.path.split(procdir)
        _, runname = os.path.split(rundir)

        self.procname = procname
        self.runname = runname
        self.kernel = kernel

        self.procdir = os.path.realpath(procdir)
        self.procstepsdir = os.path.join(self.procdir, NBRunnerNaming.steps_subdirectory)
        self.rundir_base = os.path.realpath(rundir)
        self._root_directory = os.path.realpath(root_directory)

        self.start_time = datetime.now()
        self.start_date = self.start_time.date()

        self.run_params = None
        self.nbparams = nbparams or NBRunnerNaming.default_params_filename

        if rerun is None and not latest:
            logger.debug("Initializing new nbrun '%s' (%s)", self.procdir, self.rundir_base)
            self.__first_init__()
        else:
            self.__rerun_init__(self.rundir_base, rerun, latest)

        logger.debug("Logged to file '%s/%s'", self.rundir_base, NBRunnerNaming.default_log_filename)

        self.steps = self.load_steps()
        self.filter = filter if filter is not None else ""
        self.filter_steps(self.filter, self.steps)

        self.cli_args = cli_args

    @property
    def root_directory(self):
        """ Root directory for nbrunner."""
        return self._root_directory

    def process(self):
        """Process - run all enabled  notebooks."""
        self.execute_steps(self.steps)

    def load_steps(self):
        """Load steps - list of notebooks - from procdir/steps/*.ipynb."""
        steps_pattern = os.path.join(self.procstepsdir, NBRunnerNaming.ipynb_extension_pattern)
        steps = sorted([os.path.basename(f) for f in glob.glob(steps_pattern)])
        steps = [NBRunnerStep(step) for step in steps]
        steps = {step.number: step for step in steps}
        return steps

    @staticmethod
    def parse_filter(filter):
        """Parse filter of type: 1-,^9,^12-20"""
        pattern = r'^(\^?(\d+)?(-(\d+)?)?)(,\^?(\d+)?(-(\d+)?)?)*$'
        neg = '^'
        if not re.match(pattern, filter):
            raise ValueError(f"Invalid filter '{filter}'")
        res = []
        if filter == '':
            res.append((True, None, None))
        else:
            if filter[0] == neg:
                res.append((True, None, None))
            else:
                res.append((False, None, None))
            parts = filter.split(',')
            for part in parts:
                enabled = True
                min = None
                max = None
                if len(part) > 0 and part[0] == neg:
                    enabled = False
                    part = part[1:]
                pos = part.find('-')
                if pos == -1:
                    min = int(part)
                    max = min
                else:
                    subpart1 = part[:pos] if pos > 0 else ''
                    subpart2 = part[pos+1:] if pos < len(part)-1 else ''
                    if subpart1 != '':
                        min = int(subpart1)
                    if subpart2 != '':
                        max = int(subpart2)
                res.append((enabled, min, max))
        return res

    @staticmethod
    def filter_steps(filter, steps):
        """ Filter steps according to given pattern """
        constraints = NBRunner.parse_filter(filter)
        for step_num in steps:
            for enabled, min, max in constraints:
                min = min if min is not None else -1
                max = max if max is not None else step_num + 1
                if step_num >= min and step_num <= max:
                    steps[step_num].enabled = enabled

    def __first_init__(self):
        self.run_info = self.create_next_run_directory(self.rundir_base, self.start_date)

        self.rundir = self.run_info['rundir']
        self.runstepsdir = self.run_info['stepsdir']
        reset_logging(self.rundir, NBRunnerNaming.default_log_filename)

        # duplicate params.yaml file
        src_params_filename = os.path.join(self.procdir, self.nbparams)
        self._check_file_exists(src_params_filename, '(params file)')

        dest_params_filename = os.path.join(self.rundir, NBRunnerNaming.initial_params_filename)
        copy_file(src_params_filename, dest_params_filename)

        # create initialization finished marker
        recreate_file(self.rundir, NBRunnerNaming.initialized_filename, '')

    def __rerun_init__(self, rundir_base, prev_run_name, use_latest):
        # re-initialize previous run params
        self.run_info = self.determine_run_directory(rundir_base, prev_run_name, use_latest)

        self.rundir = self.run_info['rundir']
        self.runstepsdir = self.run_info['stepsdir']
        if not file_exists(self.rundir, NBRunnerNaming.initialized_filename):
            raise ValueError("Canot rerun process, even initialization was not completed.")

        reset_logging(self.rundir, NBRunnerNaming.rerun_log_filename)

    @staticmethod
    def __build_run_directory_name(current_date: date, num: int = 0):
        res = NBRunnerNaming.cmrun_format.format(year=current_date.year, month=current_date.month, day=current_date.day, num=num)
        return res

    def create_next_run_directory(self, rundir_base, rundate):

        # create base directory for runs
        if not os.path.exists(rundir_base):
            os.makedirs(rundir_base)
            subdirs = []
        else:
            subdirs = next(os.walk(rundir_base))[1]

        # determine and create directory for this campaign's run
        proposed_name = self.__build_run_directory_name(rundate, 0)
        used_nums = [int(subdir[-3:]) for subdir in subdirs if subdir[:-3] == proposed_name[:-3]]
        if len(used_nums) > 0:
            run_number = max(used_nums)
            run_name = self.__build_run_directory_name(rundate, run_number + 1)
        else:
            run_number = 1
            run_name = proposed_name

        rundir = os.path.join(rundir_base, run_name)
        os.makedirs(rundir)
        steps_directory = os.path.join(rundir, NBRunnerNaming.steps_subdirectory)
        os.makedirs(steps_directory)

        run_info = {
            'rundir_base': rundir_base,
            'run_name': run_name,
            'run_number': run_number,
            'rundir': rundir,
            'stepsdir': steps_directory
        }

        return run_info

    @staticmethod
    def determine_run_directory(rundir_base, prev_run_name, use_latest):
        if not os.path.exists(rundir_base):
            raise ValueError('Cannot find direcctory {}.'.format(rundir_base))

        subdirs = next(os.walk(rundir_base))[1]
        if prev_run_name is not None:
            run_name = max([subdir for subdir in subdirs if subdir == prev_run_name])
        elif use_latest:
            run_name = max([subdir for subdir in subdirs])  # select latest
        else:
            raise ValueError("Cannot happen: prev_run_name: {}, use_latest: {}".format(prev_run_name, use_latest))

        run_number = int(run_name[-3:])
        rundir = os.path.join(rundir_base, run_name)
        steps_directory = os.path.join(rundir, NBRunnerNaming.steps_subdirectory)
        os.makedirs(steps_directory, exist_ok=True)

        run_info = {
            'rundir_base': rundir_base,
            'run_name': run_name,
            'run_number': run_number,
            'rundir': rundir,
            'stepsdir': steps_directory
        }
        return run_info

    def prepare_params(self, step: NBRunnerStep, prev_pickle_output_params_filename: str) -> Tuple[str, str]:
        pickle_input_params_filename = os.path.join(self.runstepsdir, step.base_name + '_input_params.pickle')
        pickle_output_params_filename = os.path.join(self.runstepsdir, step.base_name + '_output_params.pickle')
        if prev_pickle_output_params_filename is None:
            # first notebook:
            yaml_input_params_filename = os.path.join(self.rundir, 'initial_params.yaml')
            yaml_params = load_from_yaml_file(filename=yaml_input_params_filename)

            params = NBParams()
            params.update(yaml_params)
            params.process_cli_params(self.cli_args)

            save_to_pickled_file(filename=pickle_input_params_filename, content=params)
        else:
            if not file_exists(filename=pickle_input_params_filename):
                copy_file(prev_pickle_output_params_filename, pickle_input_params_filename)
            else:
                logger.debug(
                    "Params file %s for step %s already exists, not copying from previous step.",
                    pickle_input_params_filename,
                    step.name
                )

        return pickle_input_params_filename, pickle_output_params_filename

    def execute_steps(self, steps: Dict[int, NBRunnerStep]):
        prev_pickle_output_filename = None

        logger.debug("Processing all notebooks - start.")
        for step in steps:
            step_pickle_input_filename, step_pickle_output_filename = self.prepare_params(steps[step], prev_pickle_output_filename)
            if steps[step].enabled:
                self.execute_step(steps[step], step_pickle_input_filename, step_pickle_output_filename)

                if file_exists(filename=step_pickle_output_filename):
                    prev_pickle_output_filename = step_pickle_output_filename
                else:
                    logger.debug(
                        "Cannot find step_pickle_output_filename: %s, keeping previous %s",
                        step_pickle_output_filename,
                        prev_pickle_output_filename
                    )
            else:
                logger.debug(
                    "Step %s (%s) skipped due to filter pattern '%s' ",
                    step, steps[step].name,
                    self.filter
                )
                if file_exists(filename=step_pickle_output_filename):
                    prev_pickle_output_filename = step_pickle_output_filename

        logger.debug("All notebooks processed.")

    def execute_step(self, step: NBRunnerStep, step_pickle_input_filename, step_pickle_output_filename):
        source_notebook = os.path.join(self.procstepsdir, step.name)
        prepared_notebook = os.path.join(self.runstepsdir, step.base_name + NBRunnerNaming.notebook_prepared_postfix)
        processed_notebook = os.path.join(self.runstepsdir, step.base_name + NBRunnerNaming.notebook_processed_postfix)

        notebook_params = {
            'nbrundir': self.nbrundir,
            'procdir': self.procdir,
            'rundir': self.rundir,
            'step_base_name': step.base_name,
            'step_pickle_input_filename': step_pickle_input_filename,
            'step_pickle_output_filename': step_pickle_output_filename,
        }

        pmargs = {
            'progress_bar': False,
            'parameters': notebook_params,
            'prepare_only': True,
        }
        logger.debug('Kernel: %s.', self.kernel)
        if self.kernel is not None:
            pmargs['kernel_name'] = self.kernel

        logger.debug(
            "Generating prepared notebook from %s to %s (%s -> %s)",
            source_notebook,
            prepared_notebook,
            step_pickle_input_filename,
            step_pickle_output_filename
        )
        self.run_papermill(source_notebook, prepared_notebook, **pmargs)
        pmargs['prepare_only'] = False
        logger.debug(
            "Running notebook - from %s to %s (%s -> %s)",
            source_notebook,
            prepared_notebook,
            step_pickle_input_filename,
            step_pickle_output_filename
        )
        self.run_papermill(source_notebook, processed_notebook, **pmargs)
        logger.debug("Notebook %s (%s, %s).", source_notebook, prepared_notebook, processed_notebook)

    @staticmethod
    def run_papermill(source_notebook, destination_notebook, **kwargs):
        logger.debug("Executing notebook %s to %s.", source_notebook, destination_notebook)
        pm.execute_notebook(
            source_notebook,
            destination_notebook,
            **kwargs
        )
        logger.debug("Executing notebook %s to %s - finished", source_notebook, destination_notebook)

    @staticmethod
    def _check_file_exists(filename, exception_marker: str = None):
        if exception_marker is not None:
            exception_marker = ' ' + exception_marker
        if not os.path.exists(filename):
            raise ValueError(f"Filename {filename} does not exist.{exception_marker}")

        if not os.path.isfile(filename):
            raise ValueError(f"Path {filename} is not a file.{exception_marker}")
