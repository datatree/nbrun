import re


__author__ = 'Datatree'


class NBRunnerNaming:

    notebook_prepared_postfix = '_prep.ipynb'
    notebook_processed_postfix = '_run.ipynb'

    steps_subdirectory = 'steps'

    ipynb_extension_pattern = '[0-9]*.ipynb'

    default_params_filename = 'params.yaml'
    initial_params_filename = 'initial_params.yaml'

    default_log_filename = 'nbrun.log'
    rerun_log_filename = 'nbrerun.log'

    environment_file = 'env.yaml'
    params_file = 'params.yaml'
    params_pickled_file = 'params.pickled'
    steps_processed_file = 'steps_processed'
    initialized_filename = ".initialized"

    cmrun_format = "{year:04}-{month:02}-{day:02}-{num:03}"
    cmrun_pattern = re.compile(r"^[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{3}$")

    RUN = 'run'
