import logging
import os
import sys

from nbrun.nbparams import NBParams
from nbrun.naming import NBRunnerNaming
from nbrun.utils.fs import load_from_pickled_file, save_to_pickled_file, file_exists

__author__ = 'Datatree'


Myvariable = ['v', 'a', 'r']


def get_variable(variable_name, default_value=None, raise_exception=False):
    if variable_name in globals():
        value = globals()[variable_name]
    else:
        if raise_exception:
            raise ValueError(f"Cannot find variable {variable_name}.")
        else:
            value = default_value

    return value


class NBRunnerClient:

    def __init__(
            self, nbrundir, procdir, rundir, step_base_name,
            step_pickle_input_filename, step_pickle_output_filename
    ) -> None:

        self.nbrundir = nbrundir
        self.procdir = procdir
        self.rundir = rundir

        self.step_base_name = step_base_name
        self.step_pickle_input_filename = step_pickle_input_filename
        self.step_pickle_output_filename = step_pickle_output_filename

        self.proclibdir = os.path.join(self.procdir, "lib")
        if file_exists(directory=self.proclibdir) and self.proclibdir not in sys.path:
            sys.path.append(self.proclibdir)

    @property
    def stepsdir(self):
        return os.path.join(self.rundir, NBRunnerNaming.steps_subdirectory)

    @property
    def resourcesdir(self):
        return os.path.join(self.procdir, 'resources')

    def print_info(self):
        print(f"Procdir: {self.procdir}")
        print(f"Rundir: {self.rundir}")
        print(f"Step base: {self.step_base_name}")
        print(f"step_pickle_input_filename: {self.step_pickle_input_filename}")
        print(f"step_pickle_output_filename: {self.step_pickle_output_filename}")
        print("-------------------------------------")

    def params_load(self):
        if self.step_pickle_input_filename is not None:
            logging.info(f"Loading params from {self.step_pickle_input_filename}.")
            params = load_from_pickled_file(self.step_pickle_input_filename)
        else:
            logging.info(f"step_pickle_input_filename is not set, creating empty params.")
            params = NBParams()

        return params

    def params_save(self, params):
        if self.step_pickle_output_filename is not None:
            save_to_pickled_file(filename=self.step_pickle_output_filename, content=params)

    def path_to_stepsdir(self, directory, create: bool = True):
        full_directory = os.path.realpath(os.path.join(self.stepsdir, self.step_base_name, directory))
        if create:
            os.makedirs(full_directory, exist_ok=True)
        return full_directory

    def path_to_commondir(self, directory, create: bool = True):
        full_directory = os.path.realpath(os.path.join(self.rundir, directory))
        if create:
            os.makedirs(full_directory, exist_ok=True)
        return full_directory

    def build_path(self, dirspec, create: bool = True):
        if os.path.isabs(dirspec):
            full_directory = dirspec
        elif '{' in dirspec:
            full_directory = dirspec.format(
                nbrundir=self.nbrundir,
                rundir=self.rundir,
                stepsdir=os.path.join(self.stepsdir, self.step_base_name),
                resourcesdir=self.resourcesdir
            )
        else:
            full_directory = os.path.join(self.stepsdir, self.step_base_name, dirspec)

        if create:
            os.makedirs(full_directory, exist_ok=True)
        return full_directory
