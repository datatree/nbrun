import logging
import pprint
from collections import MutableMapping
from itertools import zip_longest
from typing import Any, Dict, List, Optional, Tuple

__author__ = 'Datatree'
logger = logging.getLogger(__name__)


class NBParams(MutableMapping):

    NS_NB = 'NB'
    NS_CLI = NS_NB + 'CLI'  # connadline parameters
    NS_RUN = NS_NB + 'RUN'  # loaded parameters overwritten by cli parameters

    def __init__(self, *args, **kwargs):
        self._params: Dict[str, Dict[str, Any]] = {
            self.NS_NB: {},  # internal nbrunner data
            self.NS_CLI: {},  # command-line values` CLI params preceed run params
            self.NS_RUN: {},  # parameters set during run
        }
        self._params[self.NS_RUN].update(dict(*args, **kwargs))

    def __contains__(self, key: str) -> bool:
        parts = self.split_key(key)
        pointer = self._params[self.NS_RUN]

        res = True
        for part in parts:
            if part in pointer:
                pointer = pointer[part]
            else:
                res = False
                break
        return res

    def __getitem__(self, key: str) -> Any:
        parts = self.split_key(key)
        pointer = self._params[self.NS_RUN]

        for part in parts:
            if part in pointer:
                pointer = pointer[part]  # last part
            else:
                raise KeyError(f"Provided key {key} invalid, part {part} not found in params.")

        return pointer

    def __setitem__(self, key: str, value: Any) -> None:
        parts = self.split_key(key)
        pointer = self._params[self.NS_RUN]

        for part, next_part in zip_longest(parts, parts[1:]):
            if next_part is None:
                pointer[part] = value
            else:
                if part not in pointer:
                    pointer[part] = {}
                pointer = pointer[part]

    def __delitem__(self, key: str):
        parts = self.split_key(key)
        pointers: List[Tuple[Dict[str, Any], Optional[str]]] = []
        pointer = self._params[self.NS_RUN]

        for part, next_part in zip_longest(parts, parts[1:]):
            if part in pointer:
                if next_part is not None:
                    pointers.append((pointer, part))
                    pointer = pointer[part]
                else:
                    del pointer[part]
            else:
                raise KeyError(f"Provided key {key} invalid, part {part} not found in params.")

        for pointer, part in reversed(pointers):
            if len(pointer[part]) == 0:
                del pointer[part]

    def __iter__(self):
        return iter(self._params[self.NS_RUN])

    def __len__(self):
        return len(self._params[self.NS_RUN])

    def __str__(self):
        return pprint.pformat(self._params)

    def process_cli_params(self, cli_params: Dict[str, Any]):
        self._params[self.NS_CLI] = cli_params.copy()

        for param in self._params[self.NS_CLI]:
            value = self._params[self.NS_CLI][param]
            self[param] = value

    def update(self, *args, **kwargs):
        ns = self.NS_RUN
        for key, value in dict(*args, **kwargs).items():
            self._params[ns][key] = value

    def required(self, *param_names: str) -> bool:
        for param_name in param_names:
            if param_name not in self:
                raise ValueError(f"Cannot find parameter {param_name} in current params: {str(self._params[self.NS_RUN])}.")
        return True

    @staticmethod
    def split_key(key, limit: int = None, separator: str = '.') -> List[str]:
        if limit is not None:
            parts = key.split(separator, limit)
        else:
            parts = key.split(separator)
        return parts
