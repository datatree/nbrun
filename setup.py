from setuptools import setup, find_packages
import nbrun


def get_python_extensions():
    return []


setup(
    name='nbrunner',
    version=nbrun.__version__,
    description="NBRunner - IPython notebook runner",
    classifiers=[
        'Development Status :: 5 - Production',  # 3 - Alpha, 4 - Beta, 5 - Production/Stable

        # Indicate who your project is intended for
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Analytic computation',
         'License :: Proprietary',  # Pick your license as you wish (should match "license" above)

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3.6',
    ],
    keywords='ipython automation',
    url='https://www.datatree.sk/nbrunner',
    license='Proprietary',
    author='Datatree',
    author_email='admin@datatree.sk',
    install_requires=[
        'python-dateutil == 2.6.1',
        'PyYAML == 3.12',
        'click == 6.7',
        'papermill ==  0.15.1'
    ],
    # for installation of extras use: pip install -e .[devel]
    entry_points={
        'console_scripts': [
            'nbrun = nbrun.main:nbr',
        ],
    },
    packages=find_packages(),
    ext_modules=get_python_extensions(),
)
