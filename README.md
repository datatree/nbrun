# NBRun - Notebook runner

## Předpokládaná struktura na filesystému

* `NBRUNDIR` - adresář s instalací nbrun (adresář tohoto souboru); z něj je nbrun spouštěno
* `PROCDIR` - adresář se spouštěným procesem - s notebooky, které chceme spustit
* `RUNSDIR` - adresář, do kterého se ukládají adresáře výstupy všech běhů `nbrun`
* `RUNDIR` - adresář, do kterého se ukládají výstupní soubory jednoho běhu `nbrun`


### Struktura `PROCDIR`

* `params.yaml` - soubor s parametry pro všechny notebooky
* `steps/` - adresář s notebooky
  * `steps/10_prvni.ipynb` - první notebook
  * `steps/20_prvni.ipynb` - druhý notebook
  * ...
* `resources/` - případně adresář s daty/podklady pro notebooky

### Struktura `RUNDIR`

`RUNSDIR` je adresář pro výstupy, `nbrun` jej doplňuje o identifikaci běhu ve tvaru `YYYY-MM-DD-XXX` (RUNID) do výsledného `RUNDIR`.

* `$RUNDIR/initial_params.yaml` - kopie původního params.yaml souboru z `$PROCDIR`
* `$RUNDIR/nbr.log` - logfile z běhu `nbrun`. **Pozor:** nezahrnuje logy ze samotných notebooků.
* `$RUNDIR/steps` - soubory pro jednotlivé notebooky

    * `10_prvni_input_params.pickle` - hodnoty parametrů ve stavu pro první notebook
    * `10_prvni_prepared.ipynb` - první notebook s doplněnými parametry se kterými bude spuštěn
    * `10_prvni.ipynb` - první notebook - **spuštěný**, zahrnuje i buňky s výstupy
    * `10_prvni_output_params.pickle` - hodnoty parametrů ve stavu na konci prvního notebooku
    * `20_druhy_input_params.pickle` - hodnoty parametrů ve stavu pro druhý notebook (identické s `10_prvni_output_params.yaml`)
    * `20_druhy_prepared.ipynb` - druhý notebook s doplněnými parametry se kterými bude spuštěn
    * `20_druhy.ipynb` - druhý notebook - **spuštěný**, zahrnuje i buňky s výstupy
    * `20_druhy_output_params.pickle` - hodnoty parametrů ve stavu na konci druhého notebooku
    * ...

Pozn.: `RUNSDIR` i `RUNDIR` se - pokud neexistují - při spuštění `nbrun` vytvoří.


### Běžné spouštění

```shell
nbrun resources/sample
```

Tato příkazová řádka předpokládá, že `resources/sample` (PROCDIR) obsahuje soubor `params.yaml`
a adresář `steps`, ve kterém jsou notebooky určené ke spuštění.
Dále v adresáři `resources/sample/runs` (RUNSDIR) vznikne podadresář `YYYY-MM-DD-NNN` (např. 2018-08-02-001), do kterého budou ukládány výstupy.

### Parametry pro spuštění/znovuspuštění

Běžné spuštění nepotřebuje žádné parametry. Znovuspuštění potřebuje určit, na který běh má navázat.
Pro znovuspuštění tak slouží dva parametry:
* `--rerun <RUNID>` - dojde ke znovuspuštění konkrétního běhu - tj. běhu s konkrétním YYYY-MM-DD-NNN.
* `--latest` - dojde ke znovuspuštění posledního spuštění z `RUNSDIR`.

### Parametr pro výběr notebooků

Parametr `--filter`, resp `-f` určuje, které notebooky mají být spuštěny.
Příklad:

`1,3-8,^5-6` spustí notebooky 1,3,4,7,8

`^3,^5` spustí všechny notebooky kromě notebooků 3 a 5

Jednotlivé podmínky jsou oddělené čárkou. Možné je zadat konkrétní číslo notebooku nebo rozsah od-do (včetně). Znak `^` znamená, že se notebooky v daném rozsahu nemají spuštět.

### Typické případy spouštění

#### Spuštění s přerušením

Spuštění až po notebook `30` a následně znovuspuštění od notebooku `30` (ale už bez něj) až do konce:
```shell
nbrun resources/sample --filter -30
nbrun resources/sample --latest --filter 31-
```

#### Pokračování po chybě

Předpokládejme, že spuštění
```shell
nbrun resources/sample
```
skončí chybou zpracování notebooku `30`.

Spuštění
```shell
nbrun resources/sample --latest
```
znamená spuštění všech notebooků posledního běhu od začátku, zatímco spuštění
```shell
nbrun resources/sample --latest --filter 30-
```
znamená opětovné spuštění notebooku `30` a pokračování dál.

#### Opětovné spuštění libovolného běhu

Pokud byl spuštěním
```shell
nbrun resources/sample
```
vytvořen v `RUNSDIR` adresář `2018-08-01-001`, je možné běh kdykoliv opětovně spustit

```shell
nbrun resources/sample --rerun 2018-08-01-001
```


## Notebooky

Poznámky k obsahu notebooků:

1. Z důvodů testování a ladění notebooků je vhodné vytvořit první buňku v notebooku s tagem `parameters` (v jupyteru viz menu `View -> Cell Toolbar -> Tags`), a v ní nastavit následující proměnné:

        nbrundir = ''  # directory of nbrun
        procdir = '../'   # directory with notebooks (in steps subdirs) and initial params.yaml
        rundir = '../runs'    # directory for output ($RUNSDIR/yyyy-mm-dd-xxx/... )
        step_base_name = ''  # name of the IPython notebook without the extension
        step_pickle_input_filename = ''  # name of input parametrs filename
        step_pickle_output_filename = ''  # name of output parameters filename

    Při spuštění přes `nbrun` je tato buňka zduplikována a jsou v ní nastaveny reálné hodnoty. Pokud by buňka nebyla označena tagem `parameters`, nedošlo by k aktualizaci parametrů a použily by se v běhu hodnoty proměnných uvedených přímo v notebooku.

1. Pokud notebook vyžaduje parametry modifikované některým z předchozích notebooků je třeba nainstancovat třídu `NBRunnerClient` a parametry načíst:

        import sys
        if nbrundir not in sys.path:
            sys.path.append(nbrundir)
        from nbrun.client import NBRunnerClient

        nbcl = NBRunnerClient(
            nbrundir,
            procdir,
            rundir,
            step_base_name,
            step_pickle_input_filename,
            step_pickle_output_filename,
        )
        params = nbcl.params_load()

2. Pokud v notebooku dochází k modifikaci parametrů, je nezbytné na konci notebooku parametry uložit voláním

        nbcl.params_save(params)

    Tím se uloží aktuální stav parametrů a jako takový bude dostupný pro následující notebook.


## Spouštění z cronu

V `crontab` je potřeba nastavit proměnné `LC_ALL`, `LANG`, `PYTHONPATH`,
cestu k nbrun a cesty `PROCDIR` a `RUNSDIR`.

```
LC_ALL=cs_CZ.UTF-8
NBRUNPYTHON="/home/username/projects/nbrun/.venv/bin/python"
NBRUNDIR="/home/username/projects/nbrun"
PROCDIR="/home/username/projects/nbrun/resources/sample"
RUNSDIR="/home/username/projects/nbrun/runs/sample"

45 3 * * * "${NBRUNPYTHON}" "${NBRUNDIR}/nbrun/main.py" "${PROCDIR}" "${RUNSDIR}"
```

__Upozornění__: v crontab **nefunguje** nahrazování proměnných, takže zápis `A="${B}"` nezpůsobí to, že to `A` se dosadí hodnota `B`,
ale zůstane tam řetězec `${B}`. Takže v proměnných v crontabu je potřeba hodnoty uvádět přímo.
